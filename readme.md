cd shipments<br/>
docker compose build<br/>
docker compose up<br/>

Open http://localhost:8080<br/>

Go to Add -> Create new Shipment<br/>
Go to Home Page -> Choose shipment in list -> Click edit -> Update or Delete Shipment<br/>

To run tests:<br/>
up containers<br/>
cd client<br/>
npm install<br/>
npm run test<br/>

e2e nightwatch.js test run
![](https://gitlab.com/woodyhub/cv/-/raw/main/e2e_test.PNG "e2e nightwatch.js test run")

Add Shipment
![](https://gitlab.com/woodyhub/cv/-/raw/main/add_shipment.PNG "Add Shipment")

Validation
![](https://gitlab.com/woodyhub/cv/-/raw/main/add_validation.PNG "Validation")

Api error notification
![](https://gitlab.com/woodyhub/cv/-/raw/main/api_errors.PNG "Api error notification")
